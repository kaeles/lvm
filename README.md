# Lisez-moi

## Pour les utilisateurs Windows, installez WSL
Suivez la [documentation officielle](https://docs.microsoft.com/fr-fr/windows/wsl/install-win10) de Microsoft.  

## Installation du projet

1. Clonez le dépôt
`$ git clone git@gitlab.com:rolling-web/associations/lvm.git`
2. Rendez-vous dans le sous-répertoire du thème
`$ cd [your_path_of_project]/wp-content/themes/lvm`  
3. Installez les dépendances de développement
`$ npm install`
4. Lancez un premier build
`$ npm run build && npm run start`

## Procédure de développement

N'écrivez jamais dans le fichier style.css du thème. Ce n'est pas une mauvaise pratique, mais l'architecture du thème a été pensée autrement pour faciliter le travail des développeurs. Chez LVM, le fichier style.css sert uniquement à déclarer le thème ou à gérer des hotfixs.

### Les assets 

Les assets sont les fichiers javascript et css.  
Vous trouverez ces fichiers dans le répertoire **src** du thème : `[your_path_of_project]/wp-content/themes/lvm/src`  

A terme, le thème LVM cessera d'utiliser la librarie jQuery pour la partie front. Nous souhaitons nous passer de cette librairie pour réduire considérablement la taille des assets distrubué.e.s aux clients.

1. Les fichiers javascript

Le fichier `admin.js` appelle les scripts utiles au fonctionnement des fonctionnalités personnalisées de LVM incluses dans **l'admin** de WordPress.  
Le fichier `front.js` appelle les scripts utiles au fonctionnement des fonctionnalités personnalisées de LVM incluses dans la partie **front** du site.  

La construction des assets finaux, sont gérés par [webpack](https://webpack.js.org/). Cet utilitaire lit le contenu des fichiers `admin.js` et `front.js` présents dans le répertoire `src` puis génère les fichiers de distribution présents dans le répertoire `dist` à partir des instructions contenues dans les fichiers qu'il lit.

2. Les fichiers CSS

Le fichier `editor.scss` appelle les feuilles de styles utiles à l'affichage personnalisé de l'éditeur [Gutunberg](https://developer.wordpress.org/block-editor/) pour LVM.  
Le fichier `front.scss` appelle les feuilles de styles utiles à l'affichage des éléments **front** du site. 

La construction des assets finaux sont gérés par webpack. Cette construction fonctionne de la même manière que les fichiers javascript.  

### Les builds

Le thème distribue les fichiers présent dans le répertoire `dist` et uniquement ceux qui sont minifiés. Les fichiers non minifiés sont construis pour aider au debug si nécessaire. 

Pour construire les fichiers non minifiés, depuis le répertoire du thème utilisez la commande `$ npm run start`  
Pour construire les fichiers minifiés, depuis le répertoire du thème, utilisez la commande `$ npm run build`

## Les informations incontournables relatives à WordPress

La communauté WordPress documente très bien son CMS. La documentation officielle est appelée [handbooks](https://developer.wordpress.org/).  
Avant d'écrire une fonction, assurez-vous qu'elle n'existe pas déjà en parcourant la documentation.

WordPress fonctionne de manière séquentielle et met à disposition des [hooks et filters](https://developer.wordpress.org/plugins/hooks/) pour aider les développeurs à intégrer leurs développements au bon moment dans le déroulé du processus de WordPress.  
En savoir plus sur : les [actions](https://codex.wordpress.org/Plugin_API/Action_Reference) et les [filtres](https://codex.wordpress.org/Plugin_API/Filter_Reference).


