<?php

/**
 * The template for displaying front page (used if WordPress has a homepage selected in its settings).
 *
 * @package WordPress
 * @subpackage Gear_Of_Web
 * @since Gear_Of_Web 0.1
 */
get_header();
?>

<main id="page-<?php the_ID() ?>" <?php post_class( 'landing-page' ) ?>>

	<?php if ( is_home() ) : ?>

		<article class="site-index">
			<?php get_template_part( 'template-parts/title' ); ?>
			<?php get_template_part( 'template-parts/loop', 'main' ); ?>
		</article>

	<?php else :

		get_template_part( 'template-parts/page', 'content' );

	endif;
	?>

</main>

<?php get_footer();

// END OF FILE
