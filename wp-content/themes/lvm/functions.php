<?php

// Add theme support
function lvm_theme_supports() {

	load_theme_textdomain( 'lvm_lang', get_stylesheet_directory() . '/languages' );

}
add_action( 'after_setup_theme', 'lvm_theme_supports' );

function lvm_enqueue_styles() {
    wp_enqueue_style( 'lvm.css', get_stylesheet_directory_uri() . '/dist/front.min.css', array( 'dashicons' ), '0.1', 'all' );
	wp_enqueue_script( 'lvm.js', get_stylesheet_directory_uri() . '/dist/front.min.js', null, '0.1', true );
}
add_action( 'wp_enqueue_scripts', 'lvm_enqueue_styles' );

function lvm_widgets_init() {
	if ( !file_exists( get_stylesheet_directory() . '/theme-factory/widgets/subpages-menu.php' ) ) {
		return new WP_Error( 'subpages-widget-missing', _x( 'It appears the subpages-menu.php file may be missing.', 'error log for developers', 'lvm_lang' ) );
	} else {
		require_once get_stylesheet_directory() . '/theme-factory/widgets/subpages-menu.php';
	}
}
add_action( 'after_setup_theme', 'lvm_widgets_init' );

function my_update_posts() {
    //$myposts = get_posts('showposts=-1');//Retrieve the posts you are targeting
    $args = array(
        'post_type' => array('post', 'page'),
        'numberposts' => -1
    );
    $myposts = get_posts($args);
    foreach ($myposts as $mypost){
        $mypost->post_title = $mypost->post_title.'';
        wp_update_post( $mypost );
    }
}
//add_action( 'wp_loaded', 'my_update_posts' );
