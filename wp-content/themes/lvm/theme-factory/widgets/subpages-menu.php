<?php

defined( 'ABSPATH' ) or die( 'Cheatin&#8217; uh?' );

/**
 * Display subpages of the current pages.
 */
class LVM_Sub_Pages extends WP_Widget {

	public $args = array(
		'before_title'  => '<span class="widget-title">',
		'after_title'   => '</span>',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>'
	);

	public function __construct() {
		parent::__construct(
			'lvm-sub-pages',  // Base ID
			_x( 'Subpages', 'widget name', 'lvm_lang' )   // Name
		);

		add_action( 'widgets_init', function () {
			register_widget( 'LVM_Sub_Pages' );
		});
	}

	public function widget( $args, $instance ) {
		if ( !is_page() ) {
			return;
		}

		global $post;
		$current_post = $post->ID;

		if ( $post->post_parent ) :
			$post_parent = $post->post_parent;
		else :
			$post_parent = $post->ID;
		endif;

		$query_arg = array(
			'post_type'     => 'page',
			'post_per_page' => -1,
			'post_parent'   => $post_parent,
			'oder'          => 'ASC',
			'oderby'        => 'order',
		);

		$pages = new WP_Query( $query_arg );

		if ( $pages->have_posts() ) :
			echo $args['before_widget'];
			if ( !empty( $instance['title'] ) ) {
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
			}
		?>
			<div class="submenu" aria-orientation="vertical">
				<?php while ( $pages->have_posts() ) : $pages->the_post(); ?>
					<div class="menu-item">
						<?php if ( $current_post !== get_the_ID() ) : ?>
							<a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php echo sanitize_title( get_the_title() ) ?>" class="menu-link">
								<?php the_title(); ?>
							</a>
						<?php else : ?>
							<span class="menu-link active"><?php the_title(); ?></span>
						<?php endif; ?>
					</div>
				<?php endwhile; ?>
			</div>
		<?php echo $args['after_widget'];
		endif;
		wp_reset_postdata();
	}

	public function form( $instance ) {
		$title = !empty( $instance['title'] ) ? $instance['title'] : esc_html_x( '', 'lvm_lang' ); ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php echo esc_html_x( 'Title:', 'widget input label', 'lvm_lang' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}
}
$lvm_subpages_widget = new LVM_Sub_Pages();
