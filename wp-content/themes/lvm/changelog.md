# Changelog

## 0.2

1. gear-of-web 0.6


## 0.1

1. gear-of-web 0.4
2. Widget menu des sous-pages
3. Intégration de la charte graphique
4. Support des traductions WP sur le domaine lvm_lang
