const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'production',
    entry: {
        front: {
            import: ['./src/front.js', './src/front.scss'],
        },
		admin: {
			import: ['./src/admin.js', './src/editor.scss']
		}
    },
    output: {
        filename: '[name].min.js',
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        new MiniCssExtractPlugin({
        filename: '[name].min.css',
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
    ],
    externals: {
        jquery: 'jQuery',
    },
    module: {
        rules: [
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'images/'
                }
            },
            {
                test: /\.(eot|woff|woff2|ttf)$/,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'fonts/'
                }
            },
            {
                test: /\.scss$/i,
                exclude: /node_modules/,
                use: [
                    MiniCssExtractPlugin.loader, // extracts CSS into separate files.
                    'css-loader', // interprets @import and url() like import/require() and will resolve them.
                    'postcss-loader', // Add browser prefixes and minify CSS.
                    'sass-loader', // Loads a Sass/SCSS file and compiles it to CSS
                ],
            }
        ],
    },
};
