<?php
/**
* Template Name: Landing Page
*
* @package WordPress
* @subpackage LVM Theme
* @since LVM Theme 0.2
*/
get_header();
?>

<main id="page-<?php the_ID() ?>" <?php post_class( 'landing-page' ) ?>>

    <article class="page-content">
        <?php get_template_part( 'template-parts/page', 'content' ); ?>
    </article>

</main>

<?php
get_footer();